#!/usr/bin/env python3

import time
import asyncio
from rpi_ws281x import PixelStrip, Color
from gpiozero import PWMLED,LED,Button

# LED strip configuration:
LED_COUNT1 = 9        # Number of LED pixels.
LED_PIN1 = 18

LED_COUNT2=18
LED_PIN2 = 13

LED_COUNT3=22
LED_PIN3=21  #GPIO pin connected to the pixels (18 uses PWM!).
# LED_PIN = 10        # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA = 1          # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 128  # Set to 0 for darkest and 255 for brightest
LED_INVERT = False    # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL1 = 0      # set to '1' for GPIOs 13, 19, 41, 45 or 53
LED_CHANNEL2 = 1
LED_CHANNEL3 = 0

#Vibration motor config
vib1 = LED(23)
vib2 = LED(22)

#Button
button = Button(15,False)

#Init Strips
# Create NeoPixel object with appropriate configuration.
strip1 = PixelStrip(LED_COUNT1, LED_PIN1, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL1)# Intialize the library (must be called once before other functions).
strip1.begin()
strip2 = PixelStrip(LED_COUNT2, LED_PIN2, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL2)#
strip2.begin()
strip3 = PixelStrip(LED_COUNT3, LED_PIN3, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL3)#
strip3.begin()



class easyColor:#Helper since rpi_ws281x.Colors can be set, but nor read or changed
	def __init__(self,r,g,b):
		self.color=(r,g,b)
	def toColor(self):
		return Color(self.color[0],self.color[1],self.color[2])

COLORS={
	"Pride":easyColor(213,40,35),
	"Relief":easyColor(208,221,41),
	"Sadness":easyColor(98,188,129),
	"Envy":easyColor(32,76,160),
	"Elation":easyColor(237,113,18),
	"Hope":easyColor(165,204,61),
	"Fear":easyColor(97,194,188),
	"Disgust":easyColor(57,64,150),
	"Joy":easyColor(248,135,63),
	"Interest":easyColor(96,182,48),
	"Shame":easyColor(66,158,165),
	"Contempt":easyColor(174,48,132),
	"Satisfaction":easyColor(248,235,63),
	"Surprise":easyColor(29,171,64),
	"Guilt":easyColor(71,152,188),
	"Anger":easyColor(173,48,89),
	"White":easyColor(255,255,255)
}

#used as abstraction from strips, this can be usefull, eg. when two rings are realised by the same led strip
RINGS={
	1:strip1,
	2:strip2,
	3:strip3
}


def data_to_led(entry):#takes condensed entry
	print(entry)
	ripple_single(entry)

def ripple_single(entry,pause_duration=1):#sends a ripple over the rings, based on the condensed entry
	for key, value in entry.items():
		ringColor=COLORS[key[:-6]]
		fade_duration = int(value) /4
		for i in range (3): #amount of rings
			ring = i
			fade_color(RINGS[ring+1],ringColor,duration=fade_duration)
			start_all_vibrators()
			time.sleep(pause_duration/(6-int(value)))
			stop_all_vibrators()
			fade_color(RINGS[ring+1],easyColor(0,0,0),ringColor,duration=fade_duration)

def in_between_animation():
	ripple_original({"WhiteIntens":"2"},0.3)

def ripple_original(entry,step=1):#step=time one configuration stays, entry is condesed
	
	#can be usefull for debugging
	def print_rings():
		print("R0: " , ring_0)
		print("R1: " , ring_1)
		print("R2: " , ring_2)
		print("R3: " , ring_3)
	
	emos = list(entry)
	print(emos)
	if len(emos)==0:
		return
	ring_1=easyColor(0,0,0)
	ring_2=easyColor(0,0,0)
	ring_3=easyColor(0,0,0)
	
	max_iter=len(emos)+3 #len(emos)+anzahl_ringe
	for i in range (0,max_iter):#len(emos)+anzahl_ringe
		print(i)

		
		ring_1 = COLORS[emos[i][:-6]] if i<len(emos) else easyColor(0,0,0)#[:-9] removes "Intensity"
		ring_2 = COLORS[emos[i-1][:-6]]  if  i>=1 and i<len(emos)+1 else easyColor(0,0,0)
		ring_3 = COLORS[emos[i-2][:-6]]  if  i>=2 and i<len(emos)+2 else easyColor(0,0,0)
		vib_intensity = int(entry[emos[i-2]])  if  i>=2 and i<len(emos)+2 else None
		print("in",vib_intensity)
		
		set_ring_color(1,ring_1.toColor())
		set_ring_color(2,ring_2.toColor())
		set_ring_color(3,ring_3.toColor())
		if vib_intensity == None:
			time.sleep(step)
		else:
			vibrate_total(vib_intensity,step)

def fade_all_rings(rings,pre_colors,post_colors,duration=1,smoothness=100):#takes easyColors
	
	diffs=[]
	steps=[]
	for i in range(len(rings)):
		diffs.append((post_colors[i].color[0]-pre_colors[i].color[0],post_colors[i].color[1]-pre_colors[i].color[1],post_colors[i].color[2]-pre_colors[i].color[2]))
		steps.append((diffs[i][0]/(smoothness*duration),diffs[i][1]/(smoothness*duration),diffs[i][2]/(smoothness*duration)))
	for i in range(int(smoothness*duration)):
		for j in range(len(rings)):
			_color=Color(int(steps[j][0]*i+pre_colors[j].color[0]),int(steps[j][1]*i+pre_colors[j].color[1]),int(steps[j][2]*i+pre_colors[j].color[2]))
			set_ring_color(rings[j],_color)
		time.sleep(1/smoothness)
	#Snap to final, because usage of floats may end up not resulting in perfect color
	for j in range(len(rings)):
		_color=post_colors[j].toColor()
		set_ring_color(rings[j],_color)

def loading_animation():
	ripple_original({"WhiteIntens":"2"},0.3)

def fade_color(strip,end_color,start_color=easyColor(0,0,0),duration=3,smoothness=100):#Takes in an easy color
	#CAUTION:WORKS ON STRIP LEVEL, NOT RINGS
	diff = (end_color.color[0]-start_color.color[0],end_color.color[1]-start_color.color[1],end_color.color[2]-start_color.color[2])
	step = (diff[0]/(smoothness*duration),diff[1]/(smoothness*duration),diff[2]/(smoothness*duration))
	for i in range(int(smoothness*duration)):
		_color=Color(int(step[0]*i+start_color.color[0]),int(step[1]*i+start_color.color[1]),int(step[2]*i+start_color.color[2]))
		single_color(strip,_color)
		time.sleep(1/smoothness)
	single_color(strip,end_color.toColor())

def vibrate_total(times,duration):
	for i in range(times):
		vibrate(duration/(times*2))
		time.sleep(duration/(times*2))

def vibrate(duration):
	start_all_vibrators()
	time.sleep(duration)
	stop_all_vibrators()

def stop_all_vibrators():
	vib1.off()
	vib2.off()

def start_all_vibrators():
	vib1.on()
	vib2.on()

def set_ring_color(ring, color):
	if ring == 0:
		strip1.setPixelColor(0, color)
		strip1.show()
	elif ring == 1:
		for i in range(1, strip1.numPixels()):
			strip1.setPixelColor(i, color)
		strip1.show()
	elif ring == 2:
		single_color(strip2,color)
		strip2.show()
	elif ring == 3:
		single_color(strip3,color)
		strip3.show()

def single_color(strip,color):
	for i in range(0, strip.numPixels()):
		strip.setPixelColor(i, color)
	strip.show()

def clear_strip(strip):
	for i in range(0, strip.numPixels()):
		strip.setPixelColor(i, Color(0,0,0))
	strip.show()

def clear_all():
	clear_strip(strip1)
	clear_strip(strip2)
	clear_strip(strip3)

clear_strip(strip1)
clear_strip(strip2)
clear_strip(strip3)

# Main program logic follows:
if __name__ == '__main__':
        #Only gets called when this script gets launched directly
        #used for debugging led-issues
	# Create NeoPixel object with appropriate configuration.
	strip1 = PixelStrip(LED_COUNT1, LED_PIN1, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL1)# Intialize the library (must be called once before other functions).
	strip1.begin()
	strip2 = PixelStrip(LED_COUNT2, LED_PIN2, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL2)#
	strip2.begin()
	strip3 = PixelStrip(LED_COUNT3, LED_PIN3, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL3)#
	strip3.begin()
	clear_strip(strip1)
	clear_strip(strip2)
	clear_strip(strip3)
	try:
		set_ring_color(0, Color(255, 0, 0))
		set_ring_color(1, Color(255, 255, 0))
		set_ring_color(2, Color(255, 0, 255))
		set_ring_color(3, Color(0, 0, 255))

		time.sleep(3.0)
		clear_strip(strip1)
		clear_strip(strip2)
		clear_strip(strip3)
	except KeyboardInterrupt:
		clear_strip(strip1)
		clear_strip(strip2)
		clear_strip(strip3)
