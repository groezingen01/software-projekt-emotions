#!/usr/bin/env python3

import time
from rpi_ws281x import PixelStrip, Color
#import argparse

# LED strip configuration:
LED_COUNT1 = 9        # Number of LED pixels.
LED_PIN1 = 18

LED_COUNT2=18
LED_PIN2 = 13

LED_COUNT3=22
LED_PIN3=21          # GPIO pin connected to the pixels (18 uses PWM!).
# LED_PIN = 10        # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA = 1          # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255  # Set to 0 for darkest and 255 for brightest
LED_INVERT = False    # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL1 = 0      # set to '1' for GPIOs 13, 19, 41, 45 or 53
LED_CHANNEL2 = 1
LED_CHANNEL3 = 0

def single_color(strip,color):
	for i in range(0, strip.numPixels()):
		strip.setPixelColor(i, color)
	strip.show()

def clear_strip(strip):
	for i in range(0, strip.numPixels()):
		strip.setPixelColor(i, Color(0,0,0))
	strip.show()

# Main program logic follows:
if __name__ == '__main__':
	# Create NeoPixel object with appropriate configuration.
	# Create NeoPixel object with appropriate configuration.
	strip1 = PixelStrip(LED_COUNT1, LED_PIN1, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL1)# Intialize the library (must be called once before other functions).
	strip1.begin()
	strip2 = PixelStrip(LED_COUNT2, LED_PIN2, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL2)#
	strip2.begin()
	strip3 = PixelStrip(LED_COUNT3, LED_PIN3, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL3)#
	strip3.begin()
	clear_strip(strip1)
	clear_strip(strip2)
	clear_strip(strip3)
	try:
		clear_strip(strip1)#off
	except KeyboardInterrupt:
		clear_strip(strip1)#off
    
