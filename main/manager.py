import survey_api3
import data_to_led2
import traceback
import threading


csv_string = None
fetched = threading.Event()

def api_thread():
	#Fetches the data from the api, notifyes main thread via fetched-Event
	print("Start Fetching")
	global csv_string
	csv_string=survey_api3.fetch_csv()
	fetched.set()
	return

def main():
	
	while True:
		t = threading.Thread(target=api_thread,daemon=True)
		data_to_led2.button.wait_for_press()
		#in case the button is not availabile
		#the upper line can be replaced with input() to test
		t.start()
		while not(fetched.is_set()):
			#loading animation
			data_to_led2.loading_animation()
		#clear fetching thread and event
		t.join()
		fetched.clear()

		#Send result to electronics
		if csv_string != None:
			survey_api3.csv_to_led(csv_string)

try:
	main()
except Exception as e:
	data_to_led2.clear_all()
	data_to_led2.stop_all_vibrators()
	print(traceback.format_exc())
