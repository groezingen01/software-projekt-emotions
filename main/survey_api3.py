from pathlib import Path
import traceback
from limepy import download
from gpiozero import LED,PWMLED
from io import StringIO
import csv
import time
import data_to_led2
import auth


#authentication credentials are stored in auth.py
user_name= auth.USER
user_id=auth.U_ID
password = auth.PASSWORD
base_url=auth.URL
sid=auth.SID

def csv_to_led(csv_str):
    #Create list of dicts from the csv
    csv_f = StringIO(csv_str)
    reader = csv.DictReader(csv_f,delimiter=';')
    data=[]
    for row in reader:
        data.append(row)
    
    #find last submitted entry
    last_entry=data[-1]
    i = 1
    #Skip surveys which are not submitted yet
    while(i <= len(data) and last_entry['submitdate']==''):
        last_entry=data[-i]
        i+=1
    
    print(last_entry)
    #if most recent entry was a date, show the entrys from the date
    if get_entry_type(last_entry)=='date':
        entrys=get_entrys_from_day(data,last_entry['date'])
        for entry in entrys:
            data_to_led2.data_to_led(condensed_entry(entry))
            #in-between-entrys-animation
            data_to_led2.in_between_animation()
    #if not, the last entry was an input of emotion, show those
    else:
        data_to_led2.data_to_led(condensed_entry(last_entry))

def get_entrys_from_day(data,date):#gets date as a string
    entrys = []
    for entry in data:
        if entry['datestamp'][:10]==date[:10] and get_entry_type(entry)=="feel":
            entrys.append(entry)
    return entrys

def get_entry_type(entry):
    if entry['date'] != '':
        return "date"
    else:
        return "feel"

def condensed_entry(entry):
    #removes many not set propertys from the dict
    condensed_entry={}
    for key,value in entry.items():
        if value!='' and "Intens" in key:
            condensed_entry[key]=value
    return condensed_entry

def fetch_csv():
    print('Start fetching')
    try:
        csv = download.get_responses(base_url, user_name, password, user_id, sid)
    except ValueError:
        print(traceback.format_exc())
        print("no data yet or updating")
        csv=""
    print("fetched data")
    return csv

def main():
    #Only gets excecuted if this script got launched directly
    while(True):
        try:
            csv = download.get_responses(base_url, user_name, password, user_id, sid)
            #Can be used for debugging:
            #print(csv)
            #path = Path('./responses.csv')
            #path.write_text(csv)
            csv_to_led(csv)
            time.sleep(0.5)
        except ValueError:
            print("no data yet or updating")
            csv=""
            time.sleep(2)

if "__name__"=="__main__":
    try:
        main()
    except Exception as e:
        data_to_led2.clear_all()
        data_to_led2.stop_all_vibrators()
        print(traceback.format_exc())
    
