# FEEL - Softwareprojekt WS 2022/2023

This repository holds the code for the FEEL.

> *"FEEL reminds you to notice yourself and helps you improve the understanding of your different emotions by making you frequently
> reflect on them."*

The device was created during the Softwareprojekt CodingIxD - *"Let's get Physical"*- in which students were supposed to create neo-analogue artifacts, capable of showing digital data. More information can be found here: https://www.codingixd.org/lets-get-physical/.
![FEEL device][logo]
***
[[_TOC_]]
## Interaction Scheme
There are multiple devices which are part of the interaction and data flow. In the concept, a smartwatch monitors the user's biometric parameters. If it detects changes hinting to emotional turbulence, it signals the user that such event occurred. The repository with the prototype can be found [here](https://github.com/elkazelka/fitbit_app). The user then uses an App to enter their current status of emotion. This currently is prototyped by a LimeSurvey application. At home, the user can replay and feel those emotions as well as past records using the FEEL device.

![Interaction scheme][interaction]
## Hardware structure
The FEEL device is powered by a Raspberry Pi 4 running this code. The Pi is attached to a button, 2 vibration motors and 3 LED strips. Since the LEDs need a lot of power, they are connected to an external 5V power supply. Our version of the device had a diameter of ruffly 15 cm and the LED strips were cut accordingly into 3 lengths that allow forming three circles with different diameters with in these limits. A scheme of the setup can be found here:
![Wire scheme][wire]
The encasing of the hardware was inspired by a pebble. Its top is covered in wool to get a more pleasant feel while touching it. In the middle of the top encasing, under the wool, is a whole for the button. The three LED strips are placed in circles under the ceiling. The smalls one gets poisoned in the middle, the biggest one closest to the edge. The vibration motors are glued in the position the users are expected to put their hands while holding the device.
![Top of the case][top]

The used pins can also be found in the code. The code is split up into 3 files. The data_to_led file controls the electrical devices and translates entries into a physical output. survey_api fetches the data, analyzes it, forms it to a condensed form digestible for data_to_led and calls the method to display it. The manager file awaits the user input and starts the data fetching and showing process in an endless loop.
![Programm structure][struct]
## Usage
After the setup and launch of the script, the device can play the last entered record or a date selected in the survey. The replay van be started simply by pressing the button in the middle of the device. It shows a fast white light wave, while the date gets fetched and the displays it. Each emotion gets displayed as a „ripple“ which means as a light wave. The light has the matching color of the emotion from the survey and gets faded in and out of each ring in sequence from the middle to the edge. In between the fading, there is a short waiting period in which the light effect stays. The speed of this animation depends on the intensity of the emotion. We gave more intense emotions more weight by letting them stay longer and move slower. We placed the vibration motor in the inside of the device to match the position of the user's hands. They vibrate steady during the time the light stays. This way they are synced with the animation and a small emotions' vibration feels hectic, while a large one feels more stable.
![The device in action][action]

## Setup
To set this up, wire the Pi as shown above. Clone the repository. Inside the main folder, create a file "auth.py" and declare your access credentials for the auth-variables from manager.py like this.
```python
#auth.py
#Replace empty string with your data
USER=""
U_ID=""
PASSWORD = ""
URL=""
SID=""
```
:warning: Make sure your wiring is correct. Otherwise, your components could get damaged. We give no warranty regarding the working and safety of the device.

For the LEDs to work, you activate SPI using 
```sudo raspi-config```
and follow steps 1-5 in the chapter "Vorbereitung & Installation" of [this guide.](https://tutorials-raspberrypi.de/raspberry-pi-ws2812-ws2811b-rgb-led-streifen-steuern/)
Install the led drivers with 
```pip3 install rpi_ws281x adafruit-circuitpython-neopixel```
 and the library to communicate with Limesurvey also with 
 ```pip3 install limepy```
 . Plugin the 5V power supply, always do so AFTER plugging in Pis power supply. Run manager.py with 
 ```sudo python3 manager.py```
Since an internet connection is required, you may set up an automatic wireless connection using this guide:[How to connect your Raspberry Pi to Wi-Fi](https://raspberrypihq.com/how-to-connect-your-raspberry-pi-to-wifi/).
You maybe want to launch the script after the booting of the device. You can do so using [cronjobs](https://www.makeuseof.com/how-to-run-a-raspberry-pi-program-script-at-startup/). Remember that the script need sudo rights.


>:warning: The code has been recently cleaned up to meet a deadline, but the device was not available to do testing. I tried hard to only make changes, that do not endanger the functionality of the device. If there are issues with the recent version of the program, use commit 3891f4213220c1be4ae3c5c44e92382bc340b01e. This was the last tested version.

## Credits
Yiwei Sang, Design, Kunsthochschule Weißensee
- mainly design

Elica Tokmakchieva, Computer Science, Freie Universität Berlin
- mainly survey and smartwatch

Nicolas Grözinger, Computer Science, Freie Universität Berlin
- mainly FEEL device

## License
This repo is under GPLv3, for more Details see LICENSE.

[logo]: /readme_content/cover.jpg "FEEL device"
[interaction]: readme_content/user_interaction.svg
[wire]: readme_content/schema.svg
[struct]: readme_content/ProgrammStructure.drawio.svg
[top]: readme_content/ceiling2.jpg
[action]: readme_content/in_action.gif
