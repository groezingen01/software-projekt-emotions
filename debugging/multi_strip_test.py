#!/usr/bin/env python3

import time
from rpi_ws281x import PixelStrip, Color
#import argparse

# LED strip configuration:
LED_COUNT1 = 9        # Number of LED pixels.
LED_PIN1 = 18

LED_COUNT2=18
LED_PIN2 = 13

LED_COUNT3=22
LED_PIN3=21          # GPIO pin connected to the pixels (18 uses PWM!).
# LED_PIN = 10        # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA = 1          # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 128  # Set to 0 for darkest and 255 for brightest
LED_INVERT = False    # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL1 = 0      # set to '1' for GPIOs 13, 19, 41, 45 or 53
LED_CHANNEL2 = 1
LED_CHANNEL3 = 0

class easyColor:
	def __init__(self,r,g,b):
		self.color=(r,g,b)
	def toColor(self):
		return Color(self.color[0],self.color[1],self.color[2])

def fade_in_color(strip,end_color,start_color=easyColor(0,0,0),duration=1,smoothness=100):#Takes in an easy color
	#aprupt because it always fades from 0,0,0 to goal
	diff = (end_color.color[0]-start_color.color[0],end_color.color[1]-start_color.color[1],end_color.color[2]-start_color.color[2])
	step = (diff[0]/(smoothness*duration),diff[1]/(smoothness*duration),diff[2]/(smoothness*duration))
	for i in range(smoothness*duration):
		_color=Color(int(step[0]*i+start_color.color[0]),int(step[1]*i+start_color.color[1]),int(step[2]*i+start_color.color[2]))
		single_color(strip,_color)
		time.sleep(1/smoothness)


def single_color(strip,color):
	for i in range(0, strip.numPixels()):
		strip.setPixelColor(i, color)
	strip.show()

def clear_strip(strip):
	for i in range(0, strip.numPixels()):
		strip.setPixelColor(i, Color(0,0,0))
	strip.show()

# Main program logic follows:
if __name__ == '__main__':
	# Create NeoPixel object with appropriate configuration.
	strip1 = PixelStrip(LED_COUNT1, LED_PIN1, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL1)# Intialize the library (must be called once before other functions).
	strip1.begin()
	strip2 = PixelStrip(LED_COUNT2, LED_PIN2, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL2)#
	strip2.begin()
	strip3 = PixelStrip(LED_COUNT3, LED_PIN3, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL3)#
	strip3.begin()
	clear_strip(strip1)
	clear_strip(strip2)
	clear_strip(strip3)
	try:
		"""
		fade_in_color(strip1,easyColor(255,0,0))
		fade_in_color(strip2,easyColor(0,255,0))
		fade_in_color(strip3,easyColor(0,0,255))
		time.sleep(1.0)
		fade_in_color(strip1,easyColor(0,0,0),easyColor(255,0,0))
		fade_in_color(strip2,easyColor(0,0,0),easyColor(0,255,0))
		fade_in_color(strip3,easyColor(0,0,0),easyColor(0,0,255))
		"""
		single_color(strip1,Color(255, 0, 0))#Red
		single_color(strip2,Color(0, 255, 0))
		single_color(strip3,Color(0, 0, 255))

		time.sleep(3.0)
		#single_color(strip3,Color(0, 0, 255))
		
		clear_strip(strip1)#off
		clear_strip(strip2)
		clear_strip(strip3)
	except KeyboardInterrupt:
		clear_strip(strip1)#off
		clear_strip(strip2)
		clear_strip(strip3)
