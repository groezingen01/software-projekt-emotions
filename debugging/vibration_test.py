from gpiozero import PWMLED,LED
import time

#No PWM
vib1 = LED(23)
vib2 = LED(22)
vib3 = LED(25)

vib1.on()
time.sleep(2)
vib1.off()
vib2.on()
time.sleep(2)
vib2.off()
vib3.on()
time.sleep(2)
vib3.off()
